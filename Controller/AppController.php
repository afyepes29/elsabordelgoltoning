<?php
App::uses('Controller', 'Controller');

App::import('Vendor', 'Jwt', array('file' => 'firebase' . DS . 'php-jwt' . DS . 'src' . DS . 'JWT.php'));
//App::import('Vendor', 'Fcb', array('file' => 'facebook' . DS . 'autoload.php'));

use \Firebase\JWT\JWT;

class AppController extends Controller {

	public $components = array("Session", 'RequestHandler');
	public $helpers = array('Html', 'Form', 'Text', 'Session', 'Time', 'Js' => array('Jquery'));
	
	public function beforeFilter() {
	}

}
?>
