<?php

class MainController extends AppController {

	var $uses = false;
	public $components = array( "Correo" );

	public function initData() {
		$this->Session->write( 'cedula', '' );
		$this->Session->write( 'informacionGanador', array() );
	}

	public function loginDocument() {
		//Cargamos el modelo de registro
		$this->loadModel( 'Usuario' );

		$this->initData();


		if ( $this->request->is( 'post' ) ) {

			$this->Usuario->set( $this->request->data );

			if ( $this->Usuario->loginValidate() ) {

				$cedula = $this->request->data['Usuario']['cedula'];

				$this->Session->write( 'cedula', $cedula );


				$resultado = $this->Usuario->consularRegistro( $cedula );

				if ( $resultado ) {
					$this->redirect( [ 'controller' => 'main', 'action' => 'registroCodigos' ] );
				} else {

					$session_cedula = $this->Session->read( 'cedula' );

					if ( $session_cedula == '23532116199112' ) {
						$this->redirect( [ 'controller' => 'main', 'action' => 'obtenerInformacion' ] );
					} else {
						$this->redirect( [ 'controller' => 'main', 'action' => 'registro' ] );
					}

				}

			} else {
				// didn't validate logic
				$errors = $this->Usuario->validationErrors;
			}


		}
	}

	public function registro() {

		$this->loadModel( 'Usuario' );

		//Creamos la variable cedula y le asignamos el valor enviado desde la Session
		$session_cedula = $this->Session->read( 'cedula' );

		if ( $session_cedula != null ) {

			$departamentos = $this->getDepartamentosAll();

			$this->set( 'departamentosAll', $departamentos );

			if ( $this->request->is( 'post' ) ) {

				$this->Usuario->set( $this->request->data );

				if ( $this->Usuario->registroValidate() ) {

					//prx($this->request->data);

					// Enviamos la variable cedula con los datos del formulario al modelo Usuario
					$resultadoRegistro = $this->Usuario->crearUsuario( $session_cedula, $this->request->data );

					if ( $resultadoRegistro ) {
						$this->redirect( [ 'controller' => 'main', 'action' => 'registroCodigos' ] );
					}

				} else {
					// didn't validate logic
					$errors = $this->Usuario->validationErrors;
				}
			}
		} else {
			$this->redirect( [ 'controller' => 'main', 'action' => 'loginDocument' ] );
		}

	}

	public function registroCodigos() {
		$strModelName = "Usuario";
		$this->set( 'strModelName', $strModelName );

		$this->loadModel( 'Factura' );
		$this->loadModel( 'Codigo' );

		//$this->loadModel('Premio');
		//prx($this->Premio->getPremio());

		//$this->loadModel('Usuario');
		$this->loadModel( $strModelName );

		$session_cedula = $this->Session->read( 'cedula' );

		if ( $session_cedula != null ) {

			//Consultamos los datos del usuario con la cédula de la session
			$options               = array();
			$options['conditions'] = array( 'Usuario.cedula' => $session_cedula );

			//prx($this->Usuario->find('first', $options));

			$datosUsuario = $this->Usuario->find( 'first', $options );

			$this->set( 'datosUsuario', $datosUsuario );

			// End consulta de datos del usuarios por cedula

			//Guardamos los datos del formulario de redencion de codigos
			if ( $this->request->is( 'post' ) ) {

				//prx( $this->request->data );

				//exit();

				$this->Factura->set( $this->request->data );

				if ( $this->Factura->facturaValidate() ) {

					$numero_factura = $this->request->data( 'Factura.numfactura' );
					$fecha_factura  = $this->request->data( 'Factura.fechaFactura' );

					if ( $factura_id = $this->Factura->crearFactura( $datosUsuario['Usuario']['id'], $numero_factura, $fecha_factura ) ) {

						$codigos = $this->request->data( 'Usuario.codigo' );

						foreach ( $codigos as $codigo ) {
							$this->Codigo->redimirCodigo( $factura_id, $codigo );
						}

						//Verificamos el ganador
						if ( $ganador = $this->Codigo->getGanador( $factura_id ) ) {
							$this->Session->write( 'informacionGanador', $ganador );

							//Enviamos el correo del ganador y le enviamos los datos
							$this->enviarCorreoGanador($ganador);

							$this->redirect( [ 'controller' => 'main', 'action' => 'codigoGanador' ] );

						} else {
							$this->redirect( [ 'controller' => 'main', 'action' => 'registroCodigosCompletado' ] );
						}

					} else {
						//Hubo error al crear la factura
					}


				} else {
					// Errores de validacion
					$errors = $this->Usuario->validationErrors;
				}

			}

		} else {
			$this->redirect( [ 'controller' => 'main', 'action' => 'loginDocument' ] );
		}

	}

	public function codigoGanador() {

		if ( $informacion = $this->Session->read( 'informacionGanador' ) ) {

			$this->set( 'codigo__ganador', $informacion['Codigo']['codigo'] );
			$this->set( 'factura__ganador', $informacion['Factura']['numero_factura'] );
			$this->set( 'premio__ganador', $informacion['Premio']['nombre'] );

		} else {
			$this->redirect( [ 'controller' => 'main', 'action' => 'loginDocument' ] );
		}

	}

	public function enviarCorreoGanador($datos) {

		//Capturamos los datos del usuario y los enviamos al correo
		$valores['[nombres]'] = $datos['Usuario']['nombres'];
		$valores['[apellidos]'] = $datos['Usuario']['apellidos'];
		$valores['[departamento]'] = $datos['Departamento']['nombre'];
		$valores['[ciudad]'] = $datos['Ciudad']['nombre'];
        $valores['[correo]'] = $datos['Usuario']['correo'];
		$valores['[numfactura]'] = $datos['Factura']['numero_factura'];
		$valores['[fechafactura]'] = $datos['Factura']['fecha_factura'];
		$valores['[fecharegistro]'] = $datos['Factura']['created'];
		$valores['[codigoganador]'] = $datos['Codigo']['codigo'];
		$valores['[premio]'] = $datos['Premio']['nombre'];

		$this->Correo->sendEmail( 'asistmercadeo@toningsa.com', 'NUEVOGANADOR', $valores);
		$this->Correo->sendEmail( 'juberri@hotmail.com', 'NUEVOGANADOR', $valores);
		$this->Correo->sendEmail( 'ayepes@vectorial.co', 'NUEVOGANADOR', $valores);
	}


	//Funcion para vista codigos completados pero no ganador
	public function registroCodigosCompletado() {
		//acciones
	}

	public function premiosDisponibles() {
		$this->loadModel( 'Premio' );

		$session_cedula = $this->Session->read( 'cedula' );

		if ( $session_cedula == '23532116199112' ) {

			$premios = $this->Premio->getPremiosDisponibles();

			$this->set( 'premiosDisponibles', $premios );

		} else {
			$this->redirect( [ 'controller' => 'main', 'action' => 'loginDocument' ] );
		}

	}

	public function getDepartamentosAll() {
		$this->loadModel( 'Departamento' );

		$session_cedula = $this->Session->read( 'cedula' );

		if ( $session_cedula != null || $session_cedula != '' ) {

			$listDeparamentos = $this->Departamento->getDepartamentos();

			$arrayDepartamentosNew = array();

			foreach ( $listDeparamentos as $departamentos ) {

				$contentArrayDepartamentos = $departamentos['Departamento'];// Recorremos el array departamento y lo guardamos en $contentArrayDepartamentos

				array_push( $arrayDepartamentosNew, $contentArrayDepartamentos );//Agregamos los elemenetos al nuevo arreglo que es $arrayDepartamentosNew
			}

			return $arrayDepartamentosNew;

		} else {
			$this->redirect( [ 'controller' => 'main', 'action' => 'loginDocument' ] );
		}

	}


	/**
	 *
	 */
	public function getCiudadesAll() {
		$this->autoRender = false;// Con esto le indicamos que no renderice la vista

		$this->loadModel( 'Ciudad' );

		$session_cedula = $this->Session->read( 'cedula' );

		if ( $session_cedula != null || $session_cedula != '' ) {

			$idDeparamento = $this->params['id'];

			$arrayCiudades = $this->Ciudad->getCiudades( $idDeparamento );

			$arrayCiudadesNew = array();// Inicializamos el array $arrayCiudadesNew que es el que va a contener el nuevo arreglo

			foreach ( $arrayCiudades as $ciudades ) {

				$contentArrayCiudades = $ciudades['Ciudad'];// Recorremos el array ciudad y lo guardamos en $contentArrayCiudades

				array_push( $arrayCiudadesNew, $contentArrayCiudades );//Agregamos los elemenetos al nuevo arreglo que es $arrayCiudadesNew
			}

			echo json_encode( $arrayCiudadesNew );

		} else {
			$this->redirect( [ 'controller' => 'main', 'action' => 'loginDocument' ] );
		}

	}

	public function verGanadores() {
		$this->loadModel( 'Codigo' );

		$session_cedula = $this->Session->read( 'cedula' );

		if ( $session_cedula == '23532116199112' ) {

			$ganadores = $this->Codigo->verGanadores();

			$this->set( 'listaGanadores', $ganadores );

		} else {
			$this->redirect( [ 'controller' => 'main', 'action' => 'loginDocument' ] );
		}
	}

	public function verGanadoresProgresivo() {
		$this->loadModel( 'Codigo' );

		$ganadores = $this->Codigo->verGanadoresProgresivo();

		$this->set( 'listaGanadores', $ganadores );
	}


	public function obtenerInformacion() {
		$session_cedula = $this->Session->read( 'cedula' );

		if ( $session_cedula != '23532116199112' ) {
			$this->redirect( [ 'controller' => 'main', 'action' => 'loginDocument' ] );
		}
	}

	public function listaPremios() {
		/*$session_cedula = $this->Session->read('cedula');

		if ($session_cedula == null || $session_cedula == '') {
			$this->redirect(['controller' => 'main', 'action' => 'loginDocument']);
		}*/
		$this->autoRender = true;// Con esto le indicamos que renderice la vista
	}

	public function terminosCondiciones() {
		$this->autoRender = true;// Con esto le indicamos que renderice la vista
	}

}