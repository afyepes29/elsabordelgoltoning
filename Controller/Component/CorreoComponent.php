<?php
App::uses( 'CakeEmail', 'Network/Email' );
App::uses( 'Validation', 'Utility' );

class CorreoComponent extends Component {
	public $components = array( 'Session' );

	public function sendEmail( $to, $plantilla_name, $valores, $attachments = null, $options = array(), $bandeja_salida = true, $template = 'default', $sendAs = 'html' ) {

		if ( $plantilla_name !== false ) {
			$plantilla      = ClassRegistry::init( 'Plantilla' );
			$plantilla_data = $plantilla->findByName( $plantilla_name );
			$plantilla_id   = $plantilla_data['Plantilla']['id'];
			$estado         = ( $plantilla_data['Plantilla']['estado'] == 1 );
			$asunto         = $plantilla_data['Plantilla']['asunto'];
			$mensaje        = $plantilla_data['Plantilla']['mensaje'];
			$from           = explode( ",", $plantilla_data['Plantilla']['de'] );

			foreach ( $valores AS $val => $valor ) {
				$mensaje = str_replace( $val, $valor, $mensaje );
				$asunto  = str_replace( $val, $valor, $asunto );
			}
			$estado = true;
		} else {
			$plantilla_id = null;
			$estado       = true;
			$asunto       = $valores['asunto'];
			$mensaje      = $valores['mensaje'];
		}

		//se valida si se envia
		if ( Validation::email( $to ) != '' && $estado ) {

			/** se envia el correo */
			$email = new CakeEmail( 'default' );
			$email->emailFormat( $sendAs );
			//$email->from('votreidentifiant@gmail.com');
			//$email->from( array($from[1] => $from[0]) );

			if ( is_array( $to ) ) {
				$email->to( $to['to'] );

				if ( $to['cc'] ) {
					$email->cc( $to['cc'] );
				}

				if ( $to['cc'] ) {
					$email->cc( $to['cc'] );
				}

				if ( $to['bcc'] ) {
					$email->bcc( $to['bcc'] );
				}

				if ( $to['replyTo'] ) {
					$email->replyTo( $to['replyTo'] );
				}
			} else {
				$email->to( $to );
			}

			$email->subject( $asunto );

			if ( $attachments ) {
				$email->attachments( $attachments );
			}

			//se envia
			try {
				$email->send( $mensaje );

				return true;
			} catch ( Exception $e ) {
				return false;
			}
		}

		return false;
	}

}