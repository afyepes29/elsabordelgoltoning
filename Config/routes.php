<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

	Router::connect('/', array('plugin' => false, 'controller' => 'main', 'action' => 'loginDocument'));
	Router::connect('/registro', array('plugin' => false, 'controller' => 'main', 'action' => 'registro'));
	Router::connect('/registro-codigos', array('plugin' => false, 'controller' => 'main', 'action' => 'registroCodigos'));
	Router::connect('/codigo-ganador', array('plugin' => false, 'controller' => 'main', 'action' => 'codigoGanador'));
	Router::connect('/registro-codigo-completado', array('plugin' => false, 'controller' => 'main', 'action' => 'registroCodigosCompletado'));
	Router::connect('/premios-disponibles', array('plugin' => false, 'controller' => 'main', 'action' => 'premiosDisponibles'));
	Router::connect('/ver-ganadores-admin', array('plugin' => false, 'controller' => 'main', 'action' => 'verGanadores'));
	Router::connect('/ganadores', array('plugin' => false, 'controller' => 'main', 'action' => 'verGanadoresProgresivo'));
	Router::connect('/getCiudades/:id', array('plugin' => false, 'controller' => 'main', 'action' => 'getCiudadesAll'));
	Router::connect('/administrar-informacion', array('plugin' => false, 'controller' => 'main', 'action' => 'obtenerInformacion'));
	Router::connect('/lista-de-premios', array('plugin' => false, 'controller' => 'main', 'action' => 'listaPremios'));
	Router::connect('/terminos-y-condiciones', array('plugin' => false, 'controller' => 'main', 'action' => 'terminosCondiciones'));

    //Router::connect('/visualizar-codigos', array('plugin' => false, 'controller' => 'main', 'action' => 'funcion2'));
   
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Remove this if you do not want to use
 * the built-in default routes.
 */

 
    Router::mapResources('apipartidos');
    Router::parseExtensions();
 
	require CAKE . 'Config' . DS . 'routes.php';
