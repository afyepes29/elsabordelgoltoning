<!DOCTYPE html>
<html lang="es">
<head>
    <?php echo $this->Html->charset(); ?>
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        El Sabor del Gol Toning
    </title>

    <?php echo $this->Html->meta('icon', $this->Html->url('/webroot/favicon.ico')); ?>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link href="https://fonts.googleapis.com/css?family=VT323" rel="stylesheet">
	<?php
        echo $this->Html->css( 'bootstrap.css' );
        echo $this->Html->css( 'material-kit.css' );
        echo $this->Html->css( 'main.css' );

        echo $scripts_for_layout;
    ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118781390-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-118781390-1');
    </script>
</head>
<?php
if (isset($bodyAttr)) {
	$bodyAttr = " $bodyAttr";
} else {
	$bodyAttr = null;
}
?>
<body <?php echo $bodyAttr; ?>>
<div class="container-fluid">
    <div class="row">
        <div id="content">
			<?php echo $this->Session->flash(); ?>
	        <?php if(!empty($this->params['action']) && ($this->params['action']!='codigoGanador')) : ?>
            <header>
                <nav class="navbar">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <?php if(!empty($this->params['action']) && ($this->params['action']=='loginDocument')) : ?>
                            <div class="navbar-brand">
                                <?php echo $this->Html->image('autoriza-coljuegos-home.png', array('alt' => 'Autoriza Coljuegos', 'id' => 'logo--autoriza')); ?>
                                <?php echo $this->Html->image('logo-jugarlegal.png', array('alt' => 'Autoriza Coljuegos', 'id' => 'logo--legal')); ?>
                            </div>
                            <?php endif; ?>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li id="icon--home" class="<?php echo (!empty($this->params['action']) && ($this->params['action']=='loginDocument') )?'active' :'inactive' ?>">
                                    <?php echo $this->Html->link('<i class="material-icons">home</i><span class="text--responsive">Home</span>', '/', array('escape' => false)); ?>
                                </li>
                                <li class="<?php echo (!empty($this->params['action']) && ($this->params['action']=='terminosCondiciones') )?'active' :'inactive' ?>">
                                    <?php echo $this->Html->link('Términos y condiciones', '/terminos-y-condiciones'); ?>
                                </li>
                                <li class="<?php echo (!empty($this->params['action']) && ($this->params['action']=='listaPremios') )?'active' :'inactive' ?>">
                                    <?php echo $this->Html->link('Premios', '/lista-de-premios'); ?>
                                </li>
                                <li class="<?php echo (!empty($this->params['action']) && ($this->params['action']=='verGanadoresProgresivo') )?'active' :'inactive' ?>">
                                    <?php echo $this->Html->link('Ganadores', '/ganadores'); ?>
                                </li>
                                <li id="redes">
                                    <a target="_blank" href="https://www.facebook.com/toningsa">
                                        <?php echo $this->Html->image('icono_facebook.png', array('alt' => 'Facebook')); ?>
                                    </a>
                                    <a target="_blank" href="https://www.instagram.com/alimentostoning/">
                                        <?php echo $this->Html->image('icono_instagram.png', array('alt' => 'Instragram')); ?>
                                    </a>
                                </li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </header>
	        <?php endif; ?>

            <main>
	            <?php echo $content_for_layout; ?>
            </main>

            <footer>
                <div class="container container--top">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="container__info">
                                <div class="footer--logo-toning">
                                    <?php echo $this->Html->image('logo-alimentos-toning.png', array('alt' => 'logo-toning')); ?>
                                </div>
                                <div class="footer--info">
                                    <p>
                                        Sin excepción todo ganador debe conservar el sticker y la tirilla de compra para poder reclamar el premio.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container container--bottom">
                    <?php if(!empty($this->params['action']) && ($this->params['action']=='loginDocument')) : ?>
                        <div class="row" id="copyright">
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <p>Copyright todos los derechos reservados - Alimentos Toning 2018</p>
                                <small>Línea de atención al cliente: 018000220007 | Email: servicioalcliente@toningsa.com</small>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="row" id="copyright--internas">
                            <div class="new-footer-internas col-xs-12 col-sm-12 col-md-12 text-center">
                                <p>Copyright todos los derechos reservados - Alimentos Toning 2018</p>
                                <small>Línea de atención al cliente: 018000220007 | Email: servicioalcliente@toningsa.com</small>
                            </div>
                            <!--<div class="container--table text-center">
                                <p>Copyright todos los derechos reservados - Alimentos Toning 2018</p>

                                <div class="logos--footer">
                                    <?php echo $this->Html->image('logo-jugarlegal.png', array('alt' => 'Autoriza Coljuegos', 'id' => 'footer--logo-legal')); ?>
                                    <?php echo $this->Html->image('autoriza-coljuegos-home.png', array('alt' => 'Autoriza Coljuegos', 'id' => 'footer--logo-autoriza')); ?>
                                </div>
                            </div>-->
                        </div>
                        <div class="logos--footer">
                            <?php echo $this->Html->image('logo-jugarlegal.png', array('alt' => 'Autoriza Coljuegos', 'id' => 'footer--logo-legal')); ?>
                            <?php echo $this->Html->image('autoriza-coljuegos-home.png', array('alt' => 'Autoriza Coljuegos', 'id' => 'footer--logo-autoriza')); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </footer>
        </div>
    </div>
</div>
<?php echo $this->Js->writeBuffer() ?>


<!--MATERIAL KIT-->
<!--   Core JS Files   -->
<?php echo $this->Html->script( '/js/materialkit/jquery.min.js' ); ?>
<?php echo $this->Html->script( '/js/materialkit/bootstrap.min.js' ); ?>
<?php echo $this->Html->script( '/js/materialkit/material.min.js' ); ?>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<?php echo $this->Html->script( '/js/materialkit/moment.min.js' ); ?>

<!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<?php echo $this->Html->script( '/js/materialkit/nouislider.min.js' ); ?>

<!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
<?php echo $this->Html->script( '/js/materialkit/bootstrap-datetimepicker.js' ); ?>

<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<?php echo $this->Html->script( '/js/materialkit/bootstrap-selectpicker.js' ); ?>

<!--	Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/  -->
<?php echo $this->Html->script( '/js/materialkit/bootstrap-tagsinput.js' ); ?>

<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<?php echo $this->Html->script( '/js/materialkit/jasny-bootstrap.min.js' ); ?>

<!-- Plugin For Google Maps -->
<?php //echo $this->Html->script( 'https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE' ); ?>

<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<?php echo $this->Html->script( '/js/materialkit/material-kit.js?v=1.2.1' ); ?>

<!-- Fixed Sidebar Nav - JS For Demo Purpose, Don't Include it in your project -->
<?php echo $this->Html->script( '/js/materialkit/modernizr.js' ); ?>

<?php echo $this->Html->script( '/js/materialkit/vertical-nav.js' ); ?>

<!--//Owl Carousel-->
<?php echo $this->Html->script( '/js/owl.carousel.min.js' ); ?>

<!--//Main JS-->
<?php echo $this->Html->script( '/js/main.js' ); ?>

<script type="text/javascript">

    $(document).ready(function(){

      $('#datepickerformat').datetimepicker({
          locale: 'es',
          format: 'YYYY-MM-DD'
      });

      materialKit.initFormExtendedDatetimepickers();

    });
</script>

</body>
</html>