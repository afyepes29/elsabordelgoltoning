<?php $this->set('bodyAttr', 'class="page--home"'); //Agregamos una clase única al home ?>

<div class="container-fluid" style="margin-top: 40px;">
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-12" id="column--el-sabor-del-gol">
            <?php echo $this->Html->image('logo_el-sabor-del-gol.png', array('alt' => 'Logo Toning')); ?>
            <div class="show-on-desktop no-show-on-mobile" id="promocion--valida">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <p>Promoción válida del 15 de Mayo<br> al 15 de Julio del 2018</p>
                </div>
            </div>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-12 no-padding-responsive" id="columna--form-pasos">
            <div class="step--info">
                <h1>Anota un gol y gana <br> fabulosos premios</h1>
                <span>Para participar debes completar los siguientes pasos:</span>
            </div>

            <div class="step--container">
                <div class="step--container_header">
                    <div id="stepsbar" class="stepsbar">
                        <ol>
                            <li class="active">
                                <span>Paso 1</span>
                            </li>
                            <li>
                                <span>Paso 2</span>
                            </li>
                            <li>
                                <span>Paso 3</span>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="step--container_form">
                    <?php echo $this->Form->create(); ?>
                    <?php
                    echo $this->Form->input( 'cedula', array(
                            //Peronalizamos el contenedor
                            'div' => array(
                                'class' => 'form-group',
                                'style' => 'margin-top: 0px !important;',
                            ),
                            'label' => array(
                                'class' => 'control-label',
                                'text' => 'Ingresa tu Número de Cédula'
                            ),
                            'type' => 'text',
                            'between' =>'',
                            'after'=>'',
                            'class' => 'form-control',
                            'placeholder' => 'Escribe el número sin puntos ni espacios',
                        )
                    );
                    ?>

                    <?php
                    echo $this->Form->submit('Siguiente', array(
                        'div' => array(
                            'class' => 'footer text-center',
                        ),
                        'class' => 'btn btn-lg',
                    ));
                    ?>

                    <?php echo $this->Form->end; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row no-show-on-desktop show-on-mobile" id="promocion--valida">
        <div class="col-xs-12 col-sm-12 col-md-5 text-center">
            <p>Promoción válida del 15 de Mayo<br> al 15 de Julio del 2018</p>
        </div>
    </div>

</div>