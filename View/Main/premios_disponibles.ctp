<div class="container" style="margin-top: 60px;">
    <div class="row">
        <div class="col-md-12 col-sm-12">
			<div class="card card-contact">
				<div class="header header-raised header-info text-center">
					<h4 class="card-title">Premios Disponibles Toning</h4>
				</div>
				<div class="card-content">
                    <div class="row">
                        <div class="col-md-12">
							<table class="table table-bordered">
								<tr>
									<th>Nombre del Premio</th>
									<th>Cantidad Disponible</th>
								</tr>
								
								<?php foreach($premiosDisponibles as $fila): ?>
									<tr>
										<td><?php echo $fila['Premio']['nombre']; ?></td>
										<td><?php echo $fila['0']['cantidad']; ?></td>
									</tr>
								<?php endforeach; ?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>