<section id="terminos-y-condiciones">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12 title-content no-padding">
                    <div class="col-md-3 col-sm-3 col-xs-5 logo">
                        <?php echo $this->Html->image('logo_el-sabor-del-gol.png', array('alt' => 'El Sabor del Gol Toning', 'class' => 'img-responsive')); ?>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-7 title">
                        <h1>TÉRMINOS Y CONDICIONES</h1>
                    </div>
                </div>
                <div class="col-md-12 text-style">
                    <div class="col-md-12 text-content">
                        <h2>EL SABOR DEL GOL DE ALIMENTOS TONING</h2>
                        <h3>T&Eacute;RMINOS Y CONDICIONES</h3>
                        <p> Los siguientes son los t&eacute;rminos y condiciones de la actividad promocional denominada &ldquo;EL SABOR DEL GOL&rdquo;, que realizar&aacute; ALIMENTOS TONING S.A en todo el territorio nacional colombiano y la cual estar&aacute; vigente desde el 15 de mayo de 2018 hasta el 15 de julio de 2018.</p>
                        <h3> I. MEDIOS DE DIFUSI&Oacute;N</h3>
                        <p> La promoci&oacute;n ser&aacute; difundida a trav&eacute;s de material publicitario como afiches, volantes, empaques, separatas, P.O.P en puntos de venta, redes sociales, y por medio de la p&aacute;gina web <a target="_blank" href="https://alimentostoning.com/elsabordelgol/">www.alimentostoning.com/elsabordelgol</a></p>
                        <h3> II. PARTICIPANTES</h3>
                        <p> Podr&aacute;n participar en la actividad los consumidores mayores de edad y residentes en Colombia, que cumplan con los requisitos descritos en la mec&aacute;nica de la promoci&oacute;n relacionados en el numeral IV del presente documento, y lean, comprendan y acepten todos los t&eacute;rminos y condiciones aqu&iacute; contenidos. Quedan expresamente excluidos de participar en ella las siguientes personas:</p>
                        <ol>
                            <li>Los empleados de ALIMENTOS TONING S.A, sus c&oacute;nyuges y familiares hasta el segundo grado de consanguinidad, segundo de afinidad y &uacute;nico civil.</li>
                            <li>2. Los empleados y trabajadores en misi&oacute;n de las compa&ntilde;&iacute;as proveedoras de insumos relacionados con los productos participantes, sus c&oacute;nyuges y familiares hasta el segundo grado de consanguinidad, segundo de afinidad y &uacute;nico civil.</li>
                            <li>Los empleados y trabajadores en misi&oacute;n de las agencias de publicidad e internet intervinientes en la actividad promocional, sus c&oacute;nyuges y familiares hasta el segundo grado de consanguinidad, segundo de afinidad y &uacute;nico civil.</li>
                        </ol>
                        <h3>III. PRODUCTOS QUE PARTICIPAN EN LA PROMOCI&Oacute;N</h3>
                        <p> Participan en la actividad &uacute;nicamente los productos mencionados a continuaci&oacute;n, que se encuentren identificados en los puntos de venta con el sticker de la promoci&oacute;n:</p>
                        <ul>
                            <li>AVENA HOJUELAS 1000g</li>
                            <li>HOJUELAS AZUCARADAS 1000g</li>
                            <li>CHOCO CHOC 1000g</li>
                            <li>ARITOS SABOR FRUTAS 1000g</li>
                            <li>GRANOLA PRECIO ESPECIAL 500g</li>
                            <li>MULTIGRANOLA 1000g</li>
                            <li>GRANOLA HELLO DAY CON PROTEINA 300g</li>
                            <li>GRANOLA HELLO DAY SIN AZUCAR 300g</li>
                            <li>FORZA ALMENDRAS ORIGINAL 200g</li>
                            <li>FORZA ALMENDRAS VAINILLA 200g</li>
                            <li>FORZA ALMENDRAS ORIGINAL LIQ 946ml</li>
                            <li>FORZA ALMENDRAS VAINILLA LIQ 946ml</li>
                            <li>PROTEINA INSTANT&Aacute;NEA DE SOYA 500g</li>
                        </ul>
                        <h3> IV. MEC&Aacute;NICA PROMOCIONAL</h3>
                        <p> Para participar en esta actividad, los consumidores deber&aacute;n realizar la compra de m&iacute;nimo un (1) producto de Alimentos Toning identificado con el sticker de la promoci&oacute;n, en el cual encontrar&aacute;n una clave alfanum&eacute;rica oculta por un raspa y gane. Cada clave representa una oportunidad para ganar, y el n&uacute;mero de claves a ingresar es igual a la cantidad de stickers de los productos comprados. Una vez el consumidor tenga la(s) clave(s) en su poder, deber&aacute; inscribirla(s) de la siguiente manera a trav&eacute;s de la p&aacute;gina web de la actividad:</p>
                        <ol>
                            <li>Ingrese a www.alimentostoning.com/elsabordelgol y diligencie el primer formulario en el que le solicitan sus datos personales (Nombre, Apellidos, No. C&eacute;dula, E-mail, Celular/Tel&eacute;fono, Direcci&oacute;n, Ciudad y Departamento), los cuales son obligatorios para poder contactarlo en caso de que resulte ganador.</li>
                            <li>Posteriormente diligencie los siguientes datos en los espacios indicados:
                                <ol>
                                    <li>El n&uacute;mero de la factura de compra, que tambi&eacute;n puede encontrarse como No. FV, No. Factura de Venta, No. Tiquete o Documento equivalente, y puede estar compuesto por n&uacute;meros y letras. Este n&uacute;mero solo se podr&aacute; inscribir una (1) vez durante toda la actividad.</li>
                                    <li>El n&uacute;mero de productos de Alimentos Toning comprados que se encuentran registrados en la misma factura inscrita.</li>
                                    <li>La clave alfanum&eacute;rica encontrada debajo del raspa y gane en cada sticker. Debe tener en cuenta que cada casilla en blanco corresponde a una clave diferente, y que cada clave solo se puede ingresar una (1) vez durante la vigencia de la actividad. Debe diligenciar obligatoriamente los tres (3) datos mencionados para poder participar.</li>
                                </ol>
                            </li>
                            <li>Una vez ingresados todos los datos solicitados, se activar&aacute; un bot&oacute;n que dice &ldquo;DESCUBRE EL SABOR DEL GOL&rdquo;. Proceda a dar click sobre este bot&oacute;n para saber si gana o debe seguir intent&aacute;ndolo.
                                <ol>
                                    <li>Si sale la frase GOL! GANASTE! recibir&aacute; el premio all&iacute; descrito siempre y cuando cumpla con todos los requisitos establecidos para la entrega: Presentar la c&eacute;dula original, una fotocopia de la misma, el sticker premiado original y la copia de la factura con la que result&oacute; ganador.</li>
                                    <li>Si sale la frase FUERA DE LUGAR, SIGUE INTENTANDO, ya no podr&aacute; usar esa(s) misma(s) clave(s), pero podr&aacute; volver a participar comprando otros productos identificados con el sticker de la promoci&oacute;n (mientras &eacute;sta siga vigente) y realizando nuevamente el proceso descrito anteriormente. Se podr&aacute;n registrar claves hasta las 11:59pm del 15 de julio de 2018.</li>
                                </ol>
                        </ol>
                        <p>La p&aacute;gina www.alimentostoning.com/elsabordelgol est&aacute; programada con un algoritmo que elige un ganador cada determinado n&uacute;mero de claves registradas exitosamente, teniendo en cuenta que dichas claves son &uacute;nicas e irrepetibles y no se pueden inscribir m&aacute;s de una (1) vez para poder participar. Este sistema estar&aacute; en funcionamiento durante la vigencia de la promoci&oacute;n garantizando la entrega de los 107 premios disponibles.</p>
                        <h3>V. PREMIOS QUE LA COMPA&Ntilde;&Iacute;A ENTREGAR&Aacute;</h3>
                        <p> La compa&ntilde;&iacute;a har&aacute; entrega de los siguientes premios durante la actividad promocional:</p>
                        <ul>
                            <li>25 Camisetas Adidas de la Selecci&oacute;n Colombia</li>
                            <li>20 Balones Adidas edici&oacute;n Mundial de Rusia 2018</li>
                            <li>5 Smart TV 49&rdquo;</li>
                            <li>5 Teatros en Casa</li>
                            <li>50 Bonos Adidas de $100.000</li>
                            <li>2 Tarjetas d&eacute;bito de consumo del Banco de Occidente por valor de $1.000.000</li>
                        </ul>
                        <p>Estas tarjetas son v&aacute;lidas &uacute;nicamente para pagos en establecimientos con datafonos, no para retiros en cajeros electr&oacute;nicos o transferencias; adicionalmente cada tarjeta genera un impuesto de 4x1000 que debe ser asumido por el ganador. &Eacute;stas tienen una vigencia de un (1) a&ntilde;o a partir del momento en el que el ganador haya firmado y entregado el acta de entrega del premio.</p>
                        <p> La Tarjeta se podr&aacute; reexpedir por da&ntilde;o en la banda magn&eacute;tica o p&eacute;rdida de la clave siempre y cuando el ganador conserve el pl&aacute;stico entregado inicialmente. ALIMENTOS TONING S.A. no se hace responsable por hurto, p&eacute;rdida o el uso que puedan dar los ganadores a los premios, ni por los perjuicios que dicho uso pueda ocasionar al ganador o a terceros. En caso de que aplique, ALIMENTOS TONING S.A. asumir&aacute; el pago de la ganancia ocasional correspondiente al 20% del valor del premio.</p>
                        <h3>VI. FORMA DE CONTACTO DE LOS GANADORES Y ENTREGA DE PREMIOS</h3>
                        <p> Una vez ALIMENTOS TONING S.A identifique un ganador, intentar&aacute; contactarlo telef&oacute;nicamente en 5 oportunidades, en un per&iacute;odo no superior a 72 horas posteriores a la solicitud de redenci&oacute;n a trav&eacute;s de la p&aacute;gina web de la actividad, para coordinar una fecha y un rango de hora de visita (en horas de la ma&ntilde;ana o de la tarde) donde el consumidor deber&aacute; presentar su c&eacute;dula original, una fotocopia de la misma, el sticker premiado original y la copia de la factura de compra con la que result&oacute; ganador. En caso de no ser posible la comunicaci&oacute;n telef&oacute;nica con el ganador, ALIMENTOS TONING S.A proceder&aacute; a enviar un correo electr&oacute;nico solicit&aacute;ndole que se comunique a la l&iacute;nea telef&oacute;nica 018000220007 o que indique otros n&uacute;meros de contacto donde se puede localizar; deber&aacute; responder el correo o comunicarse a la l&iacute;nea en un plazo m&aacute;ximo de 72 horas desde el momento</p>
                        <p>del env&iacute;o del mismo. Si aun habiendo enviado el correo electr&oacute;nico no es posible contactar al consumidor, el premio ser&aacute; cargado nuevamente a la disponibilidad del sitio web. Cuando el ganador sea informado de su condici&oacute;n, ALIMENTOS TONING S.A. realizar&aacute; una visita en su domicilio en un plazo no superior a tres (3) d&iacute;as h&aacute;biles para verificar que cumple con todos los requisitos de la entrega previamente mencionados en este numeral. Si el consumidor cumple efectivamente con ellos, un representante de ALIMENTOS TONING S.A. se encargar&aacute; de coordinar telef&oacute;nicamente una pr&oacute;xima cita para realizar la entrega del premio en el lugar indicado por la compa&ntilde;&iacute;a. En el momento de la entrega final, el consumidor deber&aacute; firmar un acta que lo acredita como ganador y deja constancia de que recibi&oacute; el premio a satisfacci&oacute;n. Se da la opci&oacute;n al ganador de elaborar un poder por escrito y entregarlo a ALIMENTOS TONING S.A, para autorizar que otra persona muestre los documentos solicitados para la verificaci&oacute;n e incluso reclame el premio, en caso que le sea imposible hacerlo personalmente. Es obligatorio que este poder se encuentre firmado tanto por el ganador como por el delegado, y que lleve adjunta la copia de la c&eacute;dula de ambas personas. Por otra parte, si el ganador no cumple con alguno de los requisitos de entrega, deber&aacute; firmar un acta donde consta que no cumple con todas las condiciones para obtener el premio, el cual se cargar&aacute; nuevamente a la disponibilidad en el sitio web.</p>
                        <h3> VII. ACTA DE ENTREGA</h3>
                        <p> Todo ganador, independiente del premio que haya ganado, deber&aacute; firmar obligatoriamente un acta que certifica la entrega del premio y que garantiza que lo recibi&oacute; a satisfacci&oacute;n, adem&aacute;s deber&aacute; suministrar una fotocopia de su c&eacute;dula de ciudadan&iacute;a y exhibir dicho documento en original. Cuando exista un delegado, &eacute;ste deber&aacute; firmar el acta en lugar del ganador y entregar el poder debidamente firmado con la copia de ambas c&eacute;dulas. Sin el cumplimiento de la totalidad de lo anteriormente descrito, el ganador no podr&aacute; recibir su premio. Cualquier tipo de fraude, o de intento del mismo, al igual que cualquier incumplimiento de los t&eacute;rminos y condiciones aqu&iacute; descritas, dar&aacute; lugar a que el participante sea descalificado de la actividad, sin posibilidad de poder reingresar a &eacute;sta. La compa&ntilde;&iacute;a podr&aacute; verificar el fraude, el intento de fraude, o el incumplimiento, por cualquier medio que considere pertinente.</p>
                        <h3> VIII. AUTORIZACI&Oacute;N TRATAMIENTO DE DATOS PERSONALES</h3>
                        <p> El usuario autoriza a ALIMENTOS TONING S.A a tratar los datos personales que en desarrollo de &ldquo;EL SABOR DEL GOL DE ALIMENTOS TONING&rdquo; se obtengan, con la finalidad de contactar a los ganadores, hacer entrega del premio y en general todo lo necesario para el desarrollo de esta actividad, as&iacute; como para enviar informaci&oacute;n promocional de la compa&ntilde;&iacute;a. Los datos personales que los usuarios suministren en desarrollo de la actividad promocional no podr&aacute;n ser utilizados para fines distintos de los autorizados por el propio usuario.</p>
                        <p>Se le informa al usuario que como titular de los datos personales, le asisten los siguientes derechos:</p>
                        <ol style="list-style-type: lower-alpha;">
                            <li>Conocer, actualizar y rectificar sus datos personales frente a los Responsables o Encargados.</li>
                            <li>Solicitar prueba de la autorizaci&oacute;n otorgada a los Responsables o Encargados.</li>
                            <li>Ser informado por los Responsables o Encargados, previa solicitud, respecto del uso que le ha dado a sus datos personales.</li>
                            <li>Previa queja o consulta ante los Responsables o Encargados, presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a la normatividad legal aplicable.</li>
                            <li>Revocar la autorizaci&oacute;n y/o solicitar la supresi&oacute;n del dato cuando en el Tratamiento no se respeten los principios, derechos y garant&iacute;as constitucionales y legales.</li>
                            <li>Acceder en forma gratuita a sus datos personales que hayan sido objeto de Tratamiento. Para consultas, reclamos o ampliar informaci&oacute;n los consumidores deber&aacute;n comunicarse con la l&iacute;nea de ALIMENTOS TONING S.A 018000220007</li>
                        </ol>
                        <h3>IX. DESCALIFICACI&Oacute;N</h3>
                        <p> Cualquier tipo de fraude, o de intento del mismo, al igual que cualquier incumplimiento de los t&eacute;rminos y condiciones aqu&iacute; descritas, dar&aacute; lugar a que el participante sea descalificado de la actividad, sin posibilidad de poder reingresar a &eacute;sta. La compa&ntilde;&iacute;a podr&aacute; verificar el fraude, el intento de fraude, o el incumplimiento, por cualquier medio que considere pertinente. Enti&eacute;ndase por fraude:</p>
                        <ul>
                            <li>Utilizar robots inform&aacute;ticos.</li>
                            <li>Alterar el ingreso de datos de manera ilegal.</li>
                            <li>Alterar, destruir, suprimir, robar o interceptar datos personales y de la compa&ntilde;&iacute;a.</li>
                            <li>Alterar, bloquear o realizar cualquier acto que impida mostrar o acceder a cualquier contenido, informaci&oacute;n o servicio del portal web.</li>
                            <li>Acceder de manera no autorizada al sitio web mediante t&eacute;cnicas automatizadas.</li>
                            <li>Suplantar identidades incluyendo perfiles falsos de Facebook.</li>
                            <li>Inyectar c&oacute;digo malicioso, ejecutar scripts, interceptar credenciales y sesiones, manipular objetos, archivos directorios o llaves, redireccionar el portal web hacia otros sitios web.</li>
                            <li>Realizar ataques de denegaci&oacute;n de servicio.</li>
                            <li>Utilizar software para cambiar u ocultar las direcciones IP para acceder al sitio.</li>
                            <li>Realizar an&aacute;lisis de vulnerabilidades y pruebas de intrusi&oacute;n sobre el sitio web.</li>
                            <li>Suplantar el sitio web para capturar datos personales.</li>
                        </ul>
                        <h3>VARIOS</h3>
                        <p> Las ideas, opiniones, sugerencias y comentarios que sean enviados por los concursantes a ALIMENTOS TONING S.A en forma espont&aacute;nea y sin previa solicitud, podr&aacute;n ser utilizados por &eacute;sta. Lo anterior, bajo el entendimiento de que tales comunicaciones no son confidenciales y no est&aacute;n protegidas por ninguna regulaci&oacute;n relacionada con temas de propiedad intelectual o industrial. Por ende, el remitente de tal informaci&oacute;n no podr&aacute; reclamar indemnizaci&oacute;n o participaci&oacute;n alguna en raz&oacute;n del uso comercial o extracomercial que la referida compa&ntilde;&iacute;a haga de la informaci&oacute;n en cuesti&oacute;n. ALIMENTOS TONING S.A no ser&aacute; responsable por ning&uacute;n da&ntilde;o o perjuicio que sufran, directa o indirectamente, y en conexi&oacute;n con la realizaci&oacute;n de esta actividad promocional, o con los premios ofrecidos dentro de &eacute;sta, los participantes, los ganadores y/o terceras personas, salvo que dicho da&ntilde;o o perjuicio le sea imputable a la referida compa&ntilde;&iacute;a. Este documento, y todas las relaciones de cualquier tipo que se deriven de &eacute;l, se regir&aacute;n por la ley de la Rep&uacute;blica de Colombia. ALIMENTOS TONING S.A es el titular de la p&aacute;gina web www.alimentostoning.com/elsabordelgol Para quejas, sugerencias o comentarios sobre la misma, llame a la l&iacute;nea telef&oacute;nica 018000220007 o escriba a servicioalcliente@toningsa.com Las presentes condiciones de utilizaci&oacute;n de la p&aacute;gina web, junto con aquellas que en el futuro puedan establecerse, tienen por finalidad informar a los visitantes del sitio sobre los t&eacute;rminos de la presente promoci&oacute;n y sobre la regulaci&oacute;n del uso del sitio antes referido. Se proh&iacute;be la publicaci&oacute;n en redes sociales de informaciones difamatorias, amenazantes o con contenidos que vayan contra la ley. ALIMENTOS TONING S.A se reserva el derecho de eliminar del sitio cualquier material que considere inadecuado, derecho que &eacute;ste podr&aacute; ejercer en cualquier momento. Las marcas, nombres comerciales, ense&ntilde;as, gr&aacute;ficos, dibujos, dise&ntilde;os y cualquier otra figura que constituya propiedad intelectual o industrial y que aparezca en el sitio web, est&aacute;n protegidos a favor de ALIMENTOS TONING S.A, de conformidad con las disposiciones legales sobre la materia. En consecuencia, los elementos aqu&iacute; referidos no podr&aacute;n ser utilizados, modificados, copiados, reproducidos, transmitidos o distribuidos de ninguna manera y por ning&uacute;n medio, salvo autorizaci&oacute;n previa, escrita y expresa de la compa&ntilde;&iacute;a. Los contenidos protegidos de conformidad con el p&aacute;rrafo anterior, incluyen textos, im&aacute;genes, ilustraciones, dibujos, dise&ntilde;os, software, m&uacute;sica, sonido, fotograf&iacute;as y videos, adem&aacute;s de cualquier otro medio o forma de difusi&oacute;n de dichos contenidos. Mediante la permisi&oacute;n de ingreso de los usuarios a su p&aacute;gina web, ALIMENTOS TONING S.A no est&aacute; concediendo ninguna licencia o autorizaci&oacute;n de uso de ninguna clase sobre sus derechos de</p>
                        <p>propiedad intelectual e industrial o sobre cualquier otra propiedad o derecho relacionado con su sitio o con los contenidos de &eacute;ste.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>