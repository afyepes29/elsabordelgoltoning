<div class="container" style="margin-top: 60px; width: 100%;">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="card card-contact">

                <div class="header header-raised header-info text-center">
                    <h4 class="card-title">Listado de Ganadores</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-responsive">
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Departamento</th>
                                    <th>Ciudad</th>
                                    <th>Correo</th>
                                    <th>Nombre del Premio</th>
                                    <th>Codigo Ganador</th>
                                    <th class="text-center">Número de Factura</th>
                                    <th class="text-right">Fecha de Factura</th>
                                    <th class="text-right">Fecha de Ingreso de Factura</th>
                                    <th class="text-right">Publicado</th>
                                </tr>

								<?php foreach($listaGanadores as $fila): ?>
                                    <tr>
                                        <td><?php echo $fila['Usuario']['nombres']; ?></td>
                                        <td><?php echo $fila['Usuario']['apellidos']; ?></td>
                                        <td><?php echo $fila['Departamento']['nombre']; ?></td>
                                        <td><?php echo $fila['Ciudad']['nombre']; ?></td>
                                        <td><?php echo $fila['Usuario']['correo']; ?></td>
                                        <td><?php echo $fila['Premio']['nombre']; ?></td>
                                        <td><?php echo $fila['Codigo']['codigo']; ?></td>
                                        <td class="text-center"><?php echo $fila['Factura']['numero_factura']; ?></td>
                                        <td class="text-right"><?php echo $fila['Factura']['fecha_factura']; ?></td>
                                        <td class="text-right"><?php echo $fila['Factura']['fecha_factura']; ?></td>
                                        <td class="text-right"><?php echo $fila['Codigo']['publicado']; ?></td>
                                    </tr>
								<?php endforeach; ?>
                            </table>
                        </div>
                    </div>

                    <div class="row" style="margin-top:20px;">
                        <div class="col-md-12">
                            <p class="description">
                                <a href="http://www.vectorial.co/" target="_blank">© Copyright Vectorial 2018</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>