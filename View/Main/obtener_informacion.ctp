<div class="container" style="margin-top: 100px;">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="card card-contact">
        
                <div class="header header-raised header-info text-center">
                    <h4 class="card-title">Administrador de Información</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-xs-12 col-md-3 text-center">
                            <?php echo $this->Html->link('Listado de ganadores', '/ver-ganadores-admin', array('class' => 'btn btn-success', 'target' => '_blank')); ?>
                        </div>
                        <div class="col-xs-12 col-md-3 text-center">
                            <?php echo $this->Html->link('Premios Disponibles', '/premios-disponibles', array('class' => 'btn btn-danger', 'target' => '_blank')); ?>
                        </div>
                        <div class="col-xs-12 col-md-3 text-center">
                            <?php echo $this->Html->link('Listado de Premios', '/lista-de-premios', array('class' => 'btn', 'target' => '_blank')); ?>
                        </div>
                         <div class="col-xs-12 col-md-3 text-center">
                             <?php echo $this->Html->link('Regresar a login', '/', array('class' => 'btn btn-info')); ?>
                        </div>
                    </div>

                    <div class="row" style="margin-top:20px;">
                        <div class="col-md-12">
                            <p class="description"><a href="http://www.vectorial.co/" target="_blank">© Copyright Vectorial 2018</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>