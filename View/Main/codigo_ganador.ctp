<?php $this->set('bodyAttr', 'class="page--codigo-ganador"'); //Agregamos una clase única a la página del ganador ?>

<div class="container" id="container--ganador">
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="cuadro--codigo-ganador">
                <div class="codigo-ganador-content">
	                <?php if (!empty($codigo__ganador)): ?>
                        <h4><?php echo $codigo__ganador; ?></h4>
                    <?php endif; ?>
                    <span>Código ganador</span>
	                <?php if (!empty($factura__ganador)): ?>
                        <small>Numero de Factura: <?php echo $factura__ganador; ?></small>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="informacion--ganador">
                <h2>¡HAS MARCADO UN GOLAZO!</h2>
                <span>¡GANASTE!</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6 hidden-xs hidden-sm">
            <div class="enlaces--premio text-center">
                <?php echo $this->Html->link('¿Cómo reclamar tu premio?', '/terminos-y-condiciones', array('class' => 'btn btn-reclama')); ?>
                <?php echo $this->Html->link('Volver al inicio', '/', array('class' => 'btn-volver-inicio')); ?>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="premio text-center">
                <?php
                    if(!empty($premio__ganador)){
                        $premio = $premio__ganador;
                    }else{
	                    $premio = "";
                    }
                ;
                ?>
                <?php if($premio == "CAMISETA SELECCION COLOMBIA 2018") : ?>
                    <h4>CAMISETA SELECCION<br> COLOMBIA 2018</h4>
                    <?php echo $this->Html->image('premio-camiseta-colombia.png', array('alt' => 'Premio')); ?>

                <?php elseif($premio == "BALON ADIDAS EDICION MUNDIAL RUSIA"): ?>
                    <h4>BALON ADIDAS EDICION<br> MUNDIAL RUSIA</h4>
                    <?php echo $this->Html->image('premio-balon.png', array('alt' => 'Premio')); ?>

                <?php elseif($premio == 'SMART TV 49" SAMSUNG'): ?>
                    <h4>SMART TV 49" SAMSUNG</h4>
                    <?php echo $this->Html->image('premio_smartTV.png', array('alt' => 'Premio')); ?>

                <?php elseif($premio == "TEATRO EN CASA LG"): ?>
                    <h4>TEATRO EN CASA LG</h4>
                    <?php echo $this->Html->image('premio-teatroencasa.png', array('alt' => 'Premio')); ?>

                <?php elseif($premio == "BONO REGALO ADIDAS $100.000"): ?>
                    <h4>BONO REGALO<br> ADIDAS $100.000</h4>
                    <?php echo $this->Html->image('premio-bono-adidas.png', array('alt' => 'Premio')); ?>

                <?php elseif($premio == "TARJETA DEBITO BANCO DE OCCIDENTE $1.000.000"): ?>
                    <h4>TARJETA DEBITO BANCO DE OCCIDENTE<br> $1.000.000</h4>
                    <?php echo $this->Html->image('Premio-tarjeta-banco-occidente.png', array('alt' => 'Premio')); ?>

                <?php endif; ?>
            </div>
        </div>

        <div class="col-sm-12 col-md-6 col-lg-6 visible-xs visible-sm hidden-lg hidden-md">
            <div class="enlaces--premio text-center">
                <?php echo $this->Html->link('¿Cómo reclamar tu premio?', '/terminos-y-condiciones', array('class' => 'btn btn-reclama')); ?>
                <?php echo $this->Html->link('Volver al inicio', '/', array('class' => 'btn-volver-inicio')); ?>
            </div>
        </div>
    </div>
</div>