<div class="form--generic container-fluid">
    <div class="row container-center-info">
        <div class="col-lg-4 col-md-4 col-sm-6" id="column--el-sabor-del-gol">
            <?php echo $this->Html->image('logo_el-sabor-del-gol.png', array('alt' => 'Logo Toning')); ?>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-6" id="columna--form-pasos">
            <div class="step--info">
                <h1>Anota un gol y gana <br> fabulosos premios</h1>
                <span>Para participar debes completar los siguientes pasos:</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 no-padding-responsive">
            <div class="step--container">
                <div class="step--container_header">
                    <div id="stepsbar" class="stepsbar">
                        <ol>
                            <li class="done">
                                <span>Paso 1</span>
                            </li>
                            <li class="active">
                                <span>Paso 2</span>
                            </li>
                            <li>
                                <span>Paso 3</span>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="step--container_form">
                    <span class="info--ingresadatoscompra">Ingresa tus Datos Personales</span>
					<?php echo $this->Form->create(); ?>


                    <div class="col-lg-6 col-sm-6">
						<?php
						echo $this->Form->input( 'nombres', array(
								//Peronalizamos el contenedor
								'div'   => array(
									'class' => 'form-group label-floating',
								),
								'label' => array(
									'class' => 'control-label',
									'text'  => 'Nombres *'
								),
								'class' => 'form-control',
								'type'  => 'text',
							)
						);

						?>
                    </div>

                    <div class="col-lg-6 col-sm-6">
						<?php
						echo $this->Form->input( 'apellidos', array(
								//Peronalizamos el contenedor
								'div'   => array(
									'class' => 'form-group label-floating',
								),
								'label' => array(
									'class' => 'control-label',
									'text'  => 'Apellidos *'
								),
								'class' => 'form-control',
								'type'  => 'text',
							)
						);
						?>
                    </div>

                    <div class="col-lg-6 col-sm-6">
						<?php
						echo $this->Form->input( 'telefono', array(
								//Peronalizamos el contenedor
								'div'   => array(
									'class' => 'form-group label-floating',
								),
								'label' => array(
									'class' => 'control-label',
									'text'  => 'Celular/ Teléfono *'
								),
								'class' => 'form-control',
                                'type'  => 'text',
							)
						);
						?>
                    </div>

                    <div class="col-lg-6 col-sm-6">
						<?php
						echo $this->Form->input( 'direccion', array(
								//Peronalizamos el contenedor
								'div'   => array(
									'class' => 'form-group label-floating',
								),
								'label' => array(
									'class' => 'control-label',
									'text'  => 'Dirección *'
								),
								'class' => 'form-control',
								'type'  => 'text',
							)
						);
						?>
                    </div>

                    <div class="col-lg-6 col-sm-6">

                        <select name="data[Usuario][departamento_id]" class="selectpicker" data-style="btn"
                                title="Departamento" data-size="7" id="select--departamento" tabindex="-98">
                            <option class="bs-title-option disabled" value="">Departamento</option>
							<?php foreach ( $departamentosAll as $fila ): ?>
                                <option value="<?php echo $fila['id']; ?>"><?php echo $fila['nombre']; ?></option>
							<?php endforeach; ?>
                        </select>

                    </div>

                    <div class="col-lg-6 col-sm-6">
						<?php

						echo $this->Form->input( 'ciudad_id', array(
							'label'      => false,
							'class'      => 'selectpicker',
							'id'         => 'select--ciudad',
							'data-style' => 'btn',
							'title'      => 'Ciudad',
							'data-size'  => '7',
						) );
						?>
                    </div>


                    <div class="col-lg-6 col-sm-6">
                        <?php
                        echo $this->Form->input( 'correo', array(
                                //Peronalizamos el contenedor
                                'div'   => array(
                                    'class' => 'form-group label-floating',
                                ),
                                'label' => array(
                                    'class' => 'control-label',
                                    'text'  => 'Correo electrónico *'
                                ),
                                'class' => 'form-control',
                                'type'  => 'email',
                            )
                        );
                        ?>
                    </div>


                    <div class="col-lg-12 col-sm-12">

                        <div class="checkbox">
                            <label>
								<?php echo $this->Form->input( 'terminosycondiciones', array(
									//Peronalizamos el contenedor
									'div'         => false,
									'type'        => 'checkbox',
									'between'     => '',
									'before'      => '<p>Conozo y acepto la <a target="_blank" href="https://alimentostoning.com/policies">POLÍTICA DE TRATAMIENTOS DE DATOS PERSONALES</a> de Alimentos Toning S.A., y lo autorizo a contactarme.</p>',
									'after'       => '',
									'label'       => __( 'SÍ', true ),
									'hiddenField' => false,
									'value'       => '0'
								) ); ?>
                            </label>
                        </div>
                    </div>


					<?php
					echo $this->Form->submit( 'Registrar', array(
						'div'   => array(
							'class' => 'footer text-center',
						),
						'class' => 'btn btn-primary btn-lg',
					) );
					?>
					<?php echo $this->Form->end; ?>
                </div>
            </div>

        </div>
    </div>
</div>
