<div class="container" id="container--lista-premios">
    <div class="row">
        <div class="title-content no-padding">
            <div class="col-md-3 col-sm-3 col-xs-12 logo">
                <?php echo $this->Html->image('logo_el-sabor-del-gol.png', array('alt' => 'logo', 'class' => 'img-responsive')); ?>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12 title">
                <h1>PREMIOS</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="lista--premios">
            <div class="owl-carousel owl-carousel--premios owl-theme">
                <div class="item">
                    <div class="image-destacada text-center televisores">
                        <?php echo $this->Html->image('premio_smartTV.png', array('alt' => 'Imagen')); ?>
                    </div>
                    <div class="info-premios-list">
                        <h4>TELEVISORES<br>SMART 49”</h4>
                    </div>
                </div>
                <div class="item">
                    <div class="image-destacada text-center teatro-casa">
                        <?php echo $this->Html->image('premio-teatroencasa.png', array('alt' => 'Imagen')); ?>
                    </div>
                    <div class="info-premios-list">
                        <h4>TEATROS EN CASA</h4>
                    </div>
                </div>
                <div class="item">
                    <div class="image-destacada text-center">
                        <?php echo $this->Html->image('Premio-tarjeta-banco-occidente.png', array('alt' => 'Imagen')); ?>
                    </div>
                    <div class="info-premios-list">
                        <h4>TARJETAS DÉBITO<br>$1.000.000</h4>
                    </div>
                </div>
                <div class="item">
                    <div class="image-destacada text-center">
                        <?php echo $this->Html->image('premio-camiseta-colombia.png', array('alt' => 'Imagen')); ?>
                    </div>
                    <div class="info-premios-list">
                        <h4>CAMISETAS OFICIALES<br>DE COLOMBIA</h4>
                    </div>
                </div>
                <div class="item">
                    <div class="image-destacada text-center">
                        <?php echo $this->Html->image('premio-bono-adidas.png', array('alt' => 'Imagen')); ?>
                    </div>
                    <div class="info-premios-list">
                        <h4>BONOS DE $100.000<br>TIENDA DEPORTIVA</h4>
                    </div>
                </div>
                <div class="item">
                    <div class="image-destacada text-center">
                        <?php echo $this->Html->image('premio-balon.png', array('alt' => 'Imagen')); ?>
                    </div>
                    <div class="info-premios-list">
                        <h4>BALONES OFICIALES<br>DEL MUNDIAL</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>