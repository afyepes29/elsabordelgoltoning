<?php $this->set('bodyAttr', 'class="page--register-codes"'); //Agregamos una clase única al home ?>
<div class="form--generic container-fluid">
    <div class="row container-center-info">
        <div class="col-lg-4 col-md-4 col-sm-6" id="column--el-sabor-del-gol">
            <?php echo $this->Html->image('logo_el-sabor-del-gol.png', array('alt' => 'Logo Toning')); ?>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-6" id="columna--form-pasos">
            <div class="step--info">
                <h1>Anota un gol y gana <br> fabulosos premios</h1>
                <span>Para participar debes completar los siguientes pasos:</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 no-padding-responsive">
            <div class="step--container">
                <div class="step--container_header">
                    <div id="stepsbar" class="stepsbar">
                        <ol>
                            <li class="done">
                                <span>Paso 1</span>
                            </li>
                            <li class="done">
                                <span>Paso 2</span>
                            </li>
                            <li class="done">
                                <span>Paso 3</span>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="step--container_form">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="user--name">Hola, <?php echo $datosUsuario['Usuario']['nombres']; ?></h4>
                            <span class="info--ingresadatoscompra">
                                Ingresa los datos de la compra realizada
                            </span>
                        </div>
                    </div>

					<?php echo $this->Form->create(); ?>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
							<?php
							echo $this->Form->input( 'numfactura', array(
									//Peronalizamos el contenedor
									'div'      => array(
										'class' => 'form-group label-floating has-tooltip',
									),
									'label'    => array(
										'class' => 'control-label',
										'text'  => 'Número de factura *'
									),
									'style'    => 'padding-right: 50px;',
									'class'    => 'form-control',
									'type'     => 'text',
									'required' => 'required',
                                    'before'=>'<span class="tooltip"><span><h4>Número de Factura</h4><p>También puede encontrarse como:</p><ul><li>No. FV</li><li>No. Factura de Venta</li><li>No. Tiquete </li><li>Documento equivalente.</li></ul></span></span>',
								)
							);

							?>

                            <button type="button" class="btn btn-default popup--overs" data-toggle="popover" data-placement="top" title="" data-content="" data-container="body">
                                <?php //echo $this->Html->image('icono-signo-pregunta.png', array('alt' => 'Help')); ?>
                            </button>
                        </div>

                        <div class="col-lg-3 col-md-6 col-sm-6">
							<?php
							echo $this->Form->input( 'fechaFactura', array(
									//Peronalizamos el contenedor
									'div'         => array(
										'class' => 'form-group',
										'style' => 'margin-top: -15px;',
									),
									'label'       => array(
										'class' => 'control-label',
										'text'  => 'Fecha de la factura *',
										'style' => 'margin-top: 12px; margin-bottom: 8px;',
                                        'id' => 'label-datepicker'
									),
									'class'       => 'form-control datepicker',
									'id'          => 'datepickerformat',
									'type'        => 'text',
									'placeholder' => 'Año / Mes / Día',
									'required'    => 'required',
								)
							);

							?>

                        </div>

                        <div class="col-lg-5 col-md-12 col-sm-12 has-tooltip" id="select--num-prod-compra">
                            <select class="selectpicker" id="num--productos" data-style="btn"
                                    title="Número de productos Toning comprados*" data-size="7">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                            <span class="tooltip"><span><h4>Número de productos Toning comprados</h4><p>Ingresa todos los productos de Alimentos Toning comprados que se encuentran identificados con el sticker de la promoción, y relacionados en la factura registrada en el primer campo.</p></span></span>
                            <button type="button" class="btn btn-default popup--overs overs-selects" data-toggle="popover" data-placement="top" title="" data-content=""
                                    data-container="body">
                                <?php //echo $this->Html->image('icono-signo-pregunta.png', array('alt' => 'Help')); ?>
                            </button>

                        </div>
                    </div>

                    <div class="info--code">
                        <div class="row">
                            <div class="col-sm-12"><span class="has-tooltip">Registrar clave(s) de sticker(s) <span class="tooltip"><span><h4>Registrar clave(s) de sticker(s)</h4><p>Ingresa la(s) clave(s) que encontraste en el raspa y gana de cada sticker de los productos comprados, relacionados en la factura registrada en el primer campo. Al escribir la(s) clave(s) debes tener en cuenta las mayúsculas y minúsculas, y no dejar espacios.</p></span></span></span>
                                <button type="button" class="btn btn-default popup--overs overs-inline"
                                        data-toggle="popover" data-placement="top" title="" data-content="" data-container="body">
                                    <?php //echo $this->Html->image('icono-signo-pregunta.png', array('alt' => 'Help')); ?>
                                </button>

                                <p id="no-selecto-productos">
                                    De momento no has seleccionado el número de productos Toning comprados.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <input type="hidden" id="modelname" value="<?php echo $strModelName ?>">
                        <div id="input--code"></div>

						<?php
						echo $this->Form->submit( 'Descubre si eres un ganador', array(
							'div'   => array(
								'class' => 'footer text-center',
							),
							'class' => 'btn btn-primary btn-lg',
						) );
						?>
                    </div>
					<?php echo $this->Form->end; ?>
                </div><!-- .step--container_form -->
            </div>
        </div>
    </div>