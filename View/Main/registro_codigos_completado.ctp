<div class="container container--no-ganador">
    <div class="row">
        <div class="col-md-12 col-sm-12 text-center">
            <?php echo $this->Html->image('logo_el-sabor-del-gol.png', array('alt' => 'El Sabor del Gol Toning')); ?>
        </div>

        <div class="col-md-12 col-sm-12 text-center">
            <p><strong>¡FUERA DE LUGAR!</strong><br><span>En esta ocasión tus claves no resultaron ganadoras.<br>Sigue intentándolo para ganar fabulosos premios.</span></p>

            <?php echo $this->Html->link('Descubre aquí mas sorpresas que Toning tiene para ti', 'https://alimentostoning.com/', array('class' => 'btn btn-reclama', 'target' => '_blank')); ?>
        </div>
    </div>
</div>