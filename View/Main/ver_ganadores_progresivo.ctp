<section id="ver-ganadores">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12 title-content no-padding">
                    <div class="col-md-3 col-sm-3 col-xs-5 logo">
                        <?php echo $this->Html->image('logo_el-sabor-del-gol.png', array('alt' => 'El Sabor del Gol Toning', 'class' => 'img-responsive')); ?>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-7 title">
                        <h1>GANADORES</h1>
                    </div>
                </div>

                <div class="col-md-12 table-style">
                    <div class="col-md-12 table-content">
                        <table class="table table-responsive">
                            <tr>
                                <th></th>
                                <th><p><strong>Nombres</strong></p></th>
                                <th><p><strong>Apellidos</strong></p></th>
                                <th><p><strong>Departamento</strong></p></th>
                                <th><p><strong>Ciudad</strong></p></th>
                                <th><p><strong>Premio</strong></p></th>
                            </tr>

							<?php foreach($listaGanadores as $fila): ?>
                                <tr>
                                    <td class="text-center">
                                        <?php echo $this->Html->image('trophy-cup-silhouette.png', array('alt' => 'Ganadores', 'class' => 'img-responsive')); ?>
                                    </td>
                                    <td><p><?php echo $fila['Usuario']['nombres']; ?></p></td>
                                    <td><p><?php echo $fila['Usuario']['apellidos']; ?></p></td>
                                    <td><p><?php echo $fila['Departamento']['nombre']; ?></p></td>
                                    <td><p><?php echo $fila['Ciudad']['nombre']; ?></p></td>
                                    <td><p><?php echo $fila['Premio']['nombre']; ?></p></td>
                                </tr>
							<?php endforeach; ?>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>