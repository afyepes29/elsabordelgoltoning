var $= jQuery;

$( document ).ready(function() {
    $("#num--productos").on('change', function() {

        var num_productos = $(this).val();//optenemos el valor del número de productos adquiridos
        crearInputs( num_productos );
    
    });

   /*  $('#productos--comprados').keyup( function () {
        
    }) */
});

function crearInputs(numCompras) {

    var parseCantidad = parseInt( numCompras);

    $('#input--code').html('');

    if( parseCantidad && parseCantidad <= 10 ) {
        var modelName = $("#modelname").val();

        for(i = 1; i <= parseCantidad; i++){
            var hmlt = '<div class="col-lg-6 col-sm-6">' +
                    '<div class="form-group label-floating required">' +
                        '<label for="UsuarioCodigo" class="control-label">Clave</label>\n' +
                            '<input name="data['+modelName+'][codigo][]" class="form-control" type="text" id="UsuarioCodigo" required="required">\n' +
                            '<span class="material-input">\n' +
                        '</span>\n' +
                    '</div>\n' +
                '</div>';

            $('#input--code').append(hmlt);
        }

    }
}

//Departamentos y ciudades
$(function() {
    $("#select--departamento").on('change', function() {//capturamos el evento on change del select departamento

        var id_departamento = $(this).val();//optenemos el id del departamento del select departamento

        $.get('/getCiudades/' + id_departamento, function(variable){//capturamos el id y se lo enviamos por url y lo guardamos en la variable

            $('#select--ciudad').find('option').remove();//Buscamos los option del select ciudad y li limpiamos

            var result = JSON.parse(variable);// guardamos el json en la variable result para recorrerlo

            for(var i in result){//recorremos la posicion i del json que nos devuelve
                $('#select--ciudad').append($('<option>', {
                    value: result[i].id,
                    text: result[i].nombre
                }));
            }



            $('#select--ciudad').selectpicker('refresh');//Refrescamos el select de ciudades

            //console.log(variable);
        });

    });
});

//Número de productos

$(function() {
    $("#num--productos").on('change', function() {
        $('#no-selecto-productos').remove();
    });
});
/*
$(function() {
    $("#num--productos").on('change', function() {

        $('.info--code').html(
            ''
        );
    });
});*/


//OWL CAROUSEL
/*Carousel lista--premios*/
var carousel = $(".owl-carousel--premios");
carousel.owlCarousel({
    loop:false,
    margin:10,
    responsiveClass:true,
    nav:true,
    activeClass:true,
    dots: false,
    navText: ['<i class="material-icons">keyboard_arrow_left</i>','<i class="material-icons">keyboard_arrow_right</i>'],
    responsive:{
        0:{
            items:1,
            nav:true
        },
        700:{
            items:1,
            nav:true,
        },
        900:{
            items:2,
            nav:true,
        },
        1000:{
            items:2,
            nav:true,
        },
        1280:{
            items:3,
            nav:true,
        },
        1380:{
            items:3,
            nav:true,
        },
        1440:{
            items:3,
            nav:true,
        }
    }
})

checkClasses();
carousel.on('translated.owl.carousel', function (event) {
    checkClasses();
});

function checkClasses() {
    var total = $('.owl-carousel--premios .owl-stage .owl-item.active').length;

    $('.owl-carousel--premios .owl-stage .owl-item').removeClass('firstActiveItem lastActiveItem');

    $('.owl-carousel--premios .owl-stage .owl-item.active').each(function (index) {
        if (index === 0) {
            // this is the first one
            $(this).addClass('firstActiveItem');
        }
        if (index === total - 1 && total > 1) {
            // this is the last one
            $(this).addClass('lastActiveItem');
        }
    });
}