<?php

App::uses('AppModel', 'Model');

class Codigo extends AppModel {

	//Codigo depende del modelo contador,factura y premio
	public $belongsTo = array('Contador', 'Factura', 'Premio');

	public function getGanador( $factura_id) {

		$options['conditions']=array('Codigo.factura_id' => $factura_id, 'Codigo.ganador' => 1);
		$options['fields']='Codigo.codigo, Factura.numero_factura, Factura.fecha_factura, Factura.created, Premio.nombre, Usuario.nombres, Usuario.apellidos, Usuario.correo, Departamento.nombre, Ciudad.nombre ';

		$this->bindModel( array(
				'belongsTo' => array(
					'Usuario' => array('foreignKey' => false, 'conditions' => 'Factura.usuario_id = Usuario.id'),
					'Departamento' => array('foreignKey' => false, 'conditions' => 'Departamento.id = Usuario.departamento_id'),
					'Ciudad' => array('foreignKey' => false, 'conditions' => 'Ciudad.id = Usuario.ciudad_id')
				))
		);

		$Data = $this->find('first',$options);

		return $Data;
	}
	
	public function redimirCodigo( $factura_id, $codigo ) {

		//Validar que el código este disponible
		//if($Data = $this->findByCodigoAndStatus($codigo, null)) {
		if($Data = $this->find('first', array('conditions' => array('Codigo.codigo' => $codigo, 'Codigo.status' => null)))) {			
			//Código disponible

			//Traer el consecutivo de la redencion
			$this->getModel('Contador');		
			$consecutivo = $this->Contador->siguienteConsecutivo($factura_id);
			
			$this->getModel('Parametro');	
			$ganador = $this->Parametro->findBySiguienteGanadorAndStatus($consecutivo, 0);// Realizamos la búsqueda del siguiente ganador, pasando el consecutivo y el estaus en 0.
			
			//Validar si es ganador
			if($ganador) {
				//Modificar parametros
				
				$ganador['Parametro']['status'] = 1;//Cambiamos la validacion para actualizar el status a 1 del Modelo Parametro
				$this->Parametro->guardar($ganador);//Guardamos el ganador
				
				$this->getModel('Premio');	
				$premio_id = $this->Premio->getPremio();
				
				$Data['Codigo']['ganador'] = 1;
				$Data['Codigo']['premio_id'] = $premio_id;
				
			}


			//Guardamos y asignamos el numero de factura
			$Data['Codigo']['factura_id'] = $factura_id;

			//Crear arreglo de redencion de codigo
			$Data['Codigo']['status'] = 1;
			$Data['Codigo']['contador_id'] = $consecutivo;
			
			$this->guardar($Data);
			
			return true;
			
						
		} else {
			//No esta disponible
		}
				
	}

	public function verGanadores( ){

		$options['conditions']=array('Codigo.ganador' => 1);//Realizo la condicion
		$options['fields']='Codigo.publicado, Codigo.codigo, Factura.numero_factura, Factura.fecha_factura, Factura.created, Premio.nombre, Usuario.nombres, Usuario.apellidos, Usuario.correo, Departamento.nombre, Ciudad.nombre ';//Selecciono los campos que deseo traer y cuento la cantidad y le asigno alias 'cantidad'
	
		$this->bindModel( array(
			'belongsTo' => array(
				'Usuario' => array('foreignKey' => false, 'conditions' => 'Factura.usuario_id = Usuario.id'),
				'Departamento' => array('foreignKey' => false, 'conditions' => 'Departamento.id = Usuario.departamento_id'),
				'Ciudad' => array('foreignKey' => false, 'conditions' => 'Ciudad.id = Usuario.ciudad_id')
				))
		);
		
		return $this->find('all',$options);
	}

	public function verGanadoresProgresivo( ){
		$options['conditions']=array('Codigo.ganador' => 1, 'Codigo.publicado' => 1);//Realizo la condicion
		$options['fields']='Premio.nombre, Usuario.nombres, Usuario.apellidos, Departamento.nombre, Ciudad.nombre ';//Selecciono los campos que deseo traer y cuento la cantidad y le asigno alias 'cantidad'

		$this->bindModel( array(
				'belongsTo' => array(
					'Usuario' => array('foreignKey' => false, 'conditions' => 'Factura.usuario_id = Usuario.id'),
					'Departamento' => array('foreignKey' => false, 'conditions' => 'Departamento.id = Usuario.departamento_id'),
					'Ciudad' => array('foreignKey' => false, 'conditions' => 'Ciudad.id = Usuario.ciudad_id')
				))
		);

		return $this->find('all',$options);
	}

}

?>