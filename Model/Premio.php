<?php

App::uses('AppModel', 'Model');

class Premio extends AppModel {

	public function getPremio(  ) {

		//Buscar premios disponibles
		$sSQL = "SELECT id
				FROM premios
				WHERE estado = 0
				ORDER BY RAND()
				LIMIT 1";

		if($res = $this->query($sSQL)) {
			$premio_id = $res[0]['premios']['id'];

			/* Actualizar estado = 1 a premio_id */
			$updatePremio = "UPDATE premios
							SET estado = 1
							WHERE id = $premio_id";

			$this->query($updatePremio);	

			return $premio_id;
		} else {
			return false;
		}
		//Descontar premio entregado
	}

	public function getPremiosDisponibles( ){

		$options['conditions']=array('Premio.estado' => 0);//Realizo la condicion
		$options['fields']='Premio.nombre, COUNT(*) AS cantidad';//Selecciono los campos que deseo traer y cuento la cantidad y le asigno alias 'cantidad'
		$options['group']='Premio.nombre';//Agrupo los campos por nombre

		return $this->find('all',$options);//Retorno el resultado de la consulta
	}

}

?>