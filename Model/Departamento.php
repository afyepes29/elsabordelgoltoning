<?php

App::uses('AppModel', 'Model');

class Departamento extends AppModel {

	//Retornar siguiente consecutivo
	public function getDepartamentos() {

		$options['fields']='Departamento.id, Departamento.nombre';//Selecciono los campos que deseo traer y cuento la cantidad y le asigno alias 'cantidad'

		return $this->find('all',$options);//Retorno el resultado de la consulta
	}
}

?>