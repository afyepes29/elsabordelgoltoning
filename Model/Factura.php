<?php

App::uses('AppModel', 'Model');

class Factura extends AppModel {

	public function facturaValidate() {
		$validate = array(
		    'required' => 'required',
		);

		$this->validate = $validate;
		return $this->validates();
	}


	//Retornar siguiente consecutivo
	public function crearFactura( $usuario_id, $num_factura, $fecha ) {

		$Data['usuario_id'] = $usuario_id;
		$Data['numero_factura'] = $num_factura;
		$Data['fecha_factura'] = $fecha;

		return $this->guardar( $Data ) ? $this->id : false;
	}
}

?>