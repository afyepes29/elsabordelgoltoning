<?php
App::uses('Model', 'Model');

class AppModel extends Model {
    
    var $dataSource;
    
    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        //$this->useDbConfig = getCliente();
        
        
    }
    
    public function guardar( $data )
    {
        $this->create();
        return $this->save( $data );
    }

    public function nuevo( $Data ) {
      return $this->guardar($Data);
    }

    public function modificar( $id, $Data ) {
        if(isset($Data[$this->alias])) {
            $Data[$this->alias]['id'] = $id;
        } else {
            $Data['id'] = $id;
        }

        return $this->guardar($Data);
    }
    
    public function guardarTodos( $data, $options=array() )
    {
        return $this->saveAll( $data, $options );
    }
    
    public function eliminar( $id )
    {
        $cascade=false;
        return $this->delete( $id, $cascade );
    }
    
    public function get( $id )
    {
        return $this->findById( $id );
    }
    
    public function getActivos($type='list', $conditions=array())
    {
        return $this->find($type, array('conditions' => $conditions + array($this->name .'.estado' => 1)));
    }
    
    public function getModel($model)
    {
        $this->{$model} = ClassRegistry::init( $model );
    }
    /*
    public function getList( $filtros = array(), $params = array() )
    {
        //$this->order = "Empleado.name";
        $filtros = $filtros ? $filtros : array();
        $options = array();
        $options['conditions'] = array();
        $type = isset($params['type']) ? $params['type'] : 'all';
        
        if(!isset($params['order']) && isset($this->order))
        {
            $params['order'] = $this->order;
        }
        //prx($params);
        $options['conditions'] += $filtros;
        return !isset($params['paginate']) ? $this->find($type, $options + $params) : ($options + $params);
    }
    */
    
    public function getList2( $tipo='all', $params=array() )
    {
        /*
         $condStatus = array($this->alias.'.status' => 1);
        if(isset($params['conditions'])) {
           $params['conditions'] += $condStatus; 
        } else {
            $params['conditions'] = $condStatus; 
        }
        */
        $params['recursive'] = '1';
        return $this->find( $tipo, $params );
    }
    
    protected function begin()
    {
        $this->dataSource = $this->getDataSource();
        $this->dataSource->begin( $this );
    }
    
    protected function commit()
    {
        $this->dataSource->commit( $this );
    }
    
    public function addModel($model, $conditions=NULL, $foreignKey=false, $type='hasOne')
    {
        $this->bindModel( array($type => array($model => array('foreignKey' => $foreignKey, 'conditions' => $conditions))) );
    }
    
    protected function rollback()
    {
        $this->dataSource->rollback( $this );
    }
    /*
    public function beforeDelete($cascade = true)
    {
        $papelera['model'] = $this->useTable;
        $papelera['foreign_key'] = $this->id;
        $papelera['datos'] = json_encode( $this->find('first', array('conditions' => array($this->name .'.id' => $this->id), 'recursive' => -1)) );
        
        ClassRegistry::init('Papelera')->guardar($papelera);
        
        return true;
    }
    */
    public function paginateCount($conditions = null, $recursive = 0, $extra = array())
    {
        $parameters = compact('conditions');
        $this->recursive = $recursive;
        $count = $this->find('count', array_merge($parameters, $extra));
        
        if (isset($extra['group']))
        {
            $count = $this->getAffectedRows();
        }
        
        return $count;
    }
}
?>