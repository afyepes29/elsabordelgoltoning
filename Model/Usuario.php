<?php

App::uses('AppModel', 'Model');

class Usuario extends AppModel {

	public function loginValidate() {
		$validate = array(
			'cedula' => array(
				'minLength' => array(
					'rule' => array('minLength', '6'),
					'message' => 'La cédula debe tener como minimo 6 caracteres.'
				),
				'numeric' => array(
					'rule' => 'numeric',
					'message' => 'Sólo se pueden ingresar números.'
				),
				'request_alliance' => array(
					'rule' => 'notEmpty',
					'required' => true,
					'message' => 'Debé ingresar un cédula.'
				)
			)
		);

		$this->validate = $validate;
		return $this->validates();

	}

	public function registroValidate() {
		$validate = array(
			'nombres' => array(
				'minLength' => array(
					'rule' => array('minLength', '4'),
					'message' => 'Los nombres deben tener como mínimo 3 caracteres.'
				),
				'request_alliance' => array(
					'rule' => 'notEmpty',
					'required' => true,
					'message' => 'Debé ingresar sus nombres.'
				)

			),
			'apellidos' => array(
				'minLength' => array(
					'rule' => array('minLength', '4'),
					'message' => 'Los apellidos deben tener como mínimo 4 caracteres.'
				),
				'request_alliance' => array(
					'rule' => 'notEmpty',
					'required' => true,
					'message' => 'Debé ingresar sus apellidos.'
				)

			),
			'telefono' => array(
				'minLength' => array(
					'rule' => array('minLength', '6'),
					'message' => 'El teléfono / Celular debe tener como mínimo 7 caracteres.'
				),
				'numeric' => array(
					'rule' => 'numeric',
					'message' => 'Sólo se pueden ingresar números.'
				),
				'request_alliance' => array(
					'rule' => 'notEmpty',
					'required' => true,
					'message' => 'Debé ingresar un número de Teléfono / Celular.'
				)

			),
			'direccion' => array(
				'minLength' => array(
					'rule' => array('minLength', '6'),
					'message' => 'La dirección debe tener como mínimo 5 caracteres.'
				),
				'request_alliance' => array(
					'rule' => 'notEmpty',
					'required' => true,
					'message' => 'Debé ingresar una dirección.'
				)
			),
			'departamento_id' => array(
				'request_alliance' => array(
					'rule' => 'notEmpty',
					'required' => true,
					'message' => 'Debé seleccionar el departamento donde vive.'
				)
			),
			'ciudad_id' => array(
				'request_alliance' => array(
					'rule' => 'notEmpty',
					'required' => true,
					'message' => 'Debé seleccionar la ciudad donde vive.'
				)
			),
            'correo' => array(
                'rule' => array('email', true),
                'message' => 'Proporcione una dirección de correo electrónico válida.'
            ),
			'terminosycondiciones' => array(
				'notEmpty' => array(
					'rule'     => array('comparison', '!=', 0),
					'required' => true,
					'message'  => 'Debe aceptar los términos y condiciones.'
				),
			)
		);

		$this->validate = $validate;
		return $this->validates();
	}


	//Obtenemos el documento de la base de datos
	public function consularRegistro( $cedula ) {

		$resultado = false;

		if($this->findByCedula($cedula)) {
			$resultado = true;
		}

		return $resultado;
	}


	public function crearUsuario( $cedula, $Data ) {

		$Data['Usuario']['cedula'] = $cedula;
		//prx($Data);

		return $this->guardar( $Data );
	}
}

?>
