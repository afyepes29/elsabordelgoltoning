<?php

App::uses('AppModel', 'Model');

class Ciudad extends AppModel {

	//Retornar siguiente consecutivo
	public function getCiudades( $deparamento_id ) {

		$options['conditions']=array('Ciudad.departamento_id' => $deparamento_id);//Realizo la condicion
		$options['fields']='Ciudad.id, Ciudad.nombre, Ciudad.departamento_id';//Selecciono los campos que deseo traer y cuento la cantidad y le asigno alias 'cantidad'

		return $this->find('all',$options);//Retorno el resultado de la consulta
	}
}

?>