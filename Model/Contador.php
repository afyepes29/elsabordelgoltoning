<?php

App::uses('AppModel', 'Model');

class Contador extends AppModel {

	//Retornar siguiente consecutivo
	public function siguienteConsecutivo( $factura_id ) {

		$Data['factura_id'] = $factura_id;
		
		return $this->guardar( $Data ) ? $this->id : false;// Si Data es success return id de lo contrario return false
	}
}

?>