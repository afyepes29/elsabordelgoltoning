<?php
App::uses('AppController', 'Controller');

App::uses('AppShell', 'Console/Command');
App::uses('ComponentCollection', 'Controller');
App::uses('Controller', 'Controller');
App::uses('CorreoComponent', 'Controller/Component');

class ProyectosShell extends AppShell
{
    var $uses = array('Tarea');
		
    public function tareas_vencidas() {
      
        $collection = new ComponentCollection();
        $this->Correo = new CorreoComponent( $collection );
      

		$arrFrom[ Configure::read('vsSmartCrm.from_email') ] = Configure::read('vsSmartCrm.from_name');

		$lstTareasAbiertasUsuario = Set::Combine($this->Tarea->find('all', array('conditions' => array('Tarea.status' => TASK_OPEN, 'Tarea.due_date <' => date("Y-m-d")), 'order' => 'Tarea.responsable_id')),
										  '{n}.Tarea.id',
										  '{n}',
										  '{n}.Tarea.responsable_id');
		
		foreach($lstTareasAbiertasUsuario as $tareas) {
			$Mensaje = '<ul>';
			foreach($tareas as $tarea) {
				$lstTo[ $tarea['Responsable']['email'] ] = $tarea['Responsable']['first_name'].' '.$tarea['Responsable']['last_name'];
				$Mensaje .= '<li>'.$tarea['Tarea']['subject'].'. <strong>Venció: '.expresionFecha($tarea['Tarea']['due_date'], 'mes_dia').'</strong></li>';
			}
			$Mensaje .= '</ul>';

          /*** Enviar notificacion ***/
            $arrValores['Tareas']['name'] = $Mensaje;
            
            $this->Correo->enviar( $lstTo, array('valores' => $arrValores, 'plantilla' => PLNT_TAREAS_VENCIDAS), $arrFrom);
          /***************************/
        }

    }

}
?>